package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		int nextNum = verseNumber -1;
		if (nextNum == 1) {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + nextNum + " bottle of beer on the wall.\n";
		}
		else if (nextNum == 0) {
			return verseNumber + " bottle of beer on the wall, " + verseNumber + " bottle of beer.\n"
					+ "Take it down and pass it around, no more bottles of beer on the wall.\n";
		}
		else if (verseNumber == 0) {
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}
		else  {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + nextNum + " bottles of beer on the wall.\n";
		}
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		String song = verse(startVerseNumber);
		for (int i = startVerseNumber-1; i >= endVerseNumber; i--) {
			song = song + "\n" + verse(i);
		}
		return song;
	}

	public String song() {
		String song = verse(99);
		int startNum = 98;
		int endNum = 0;
		for (int i = startNum; i >= endNum; i--) {
			song = song + "\n" + verse(i);
		}
		return song;
	}
}
